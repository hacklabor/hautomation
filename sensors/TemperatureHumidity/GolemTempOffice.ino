/*
Für NodeMCU mit angeschlossenem Adafruit-Display SSD1306
Von Soeren Fuhrmann
*/

// ESP8266
#include <ESP8266WiFi.h>
#include <WiFiClient.h>

// DS18B20
#include <DallasTemperature.h>

// Display
#include <Wire.h>
#include <SSD1306.h>

// DHT22
#include <Adafruit_Sensor.h>
#include <DHT.h>
#include <DHT_U.h>

// MQTT
#include <PubSubClient.h>

// DHT22
#define DHTPIN            D1        // Pin which is connected to the DHT sensor.
#define DHTTYPE           DHT22     // DHT 22 (AM2302)

DHT_Unified dht(DHTPIN, DHTTYPE);

// Display
#define ADDRESS 0x3C
#define SDA D2
#define SDC D3
SSD1306  display(ADDRESS, SDA, SDC);

// Die URL sollte über das Formular erzeugt werden:
// http://www.golem.de/projekte/ot/doku.php#urlgen
// * Der Servername darf nicht enthalten sein
// * "Temperatur anhaengen" auswählen
#define URL "/projekte/ot/temp.php?dbg=0&token=5bb8a41c301f8b7af5f7bbde3b77de0d&city=Schwerin&zip=19061&country=DE&lat=53.60&long=11.41&type=wifimcu&temp="

// Wlan-Daten: Name des Netzwerks (SSID) und Passwort
#define SSID "HierDeineSSIDEinfuegen"
#define PASSWORT "DasPasswortIstGeheim"

// Zeitperiode, alle 15 Minuten (maximal 20 Zeichen auf dem Display)
#define PERIOD 15
static uint8_t minutes = 1;

// Pin für den Thermometer-Sensor
#define ONE_WIRE_BUS D4 // D4

OneWire oneWire(ONE_WIRE_BUS);
DallasTemperature sensors(&oneWire);
DeviceAddress thermometer;

WiFiClient client;

// MQTT
//const char* mqtt_server = "10.42.255.248";
const char* mqtt_server = "hautomation01.hacklabor.de";
PubSubClient MQTTclient(client);

void reconnect()
{
  // Loop until we're reconnected
  while (!MQTTclient.connected())
  {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (MQTTclient.connect("ESP8266Client"))
    {
      Serial.println("connected");
    }
    else
    {
      Serial.print("failed, rc=");
      Serial.print(MQTTclient.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

void connectWifi()
{
  int xpos = 0;
  
  WiFi.disconnect();
  WiFi.mode(WIFI_STA);
  WiFi.begin(SSID, PASSWORT);

  display.clear();
  display.drawString(0, 0, "connect wifi");
  display.display();
  Serial.print("connect wifi");

  while (WiFi.status() != WL_CONNECTED)
  {
    display.drawString(xpos, 16, ".");
    xpos += 4;
    display.display();
    Serial.print(".");
    delay(500);
  }

  display.clear();
  display.drawString(0, 0, "wifi connected");
  display.display();
  Serial.println();
  Serial.print("wifi ip: ");
  Serial.println(WiFi.localIP());

  delay(3000);
}

void setup()
{
  Serial.begin(115200);
  Serial.println();

  display.init();
  display.clear();
  
  display.setFont(ArialMT_Plain_16);
  
  connectWifi();

  // DS18B20
  sensors.begin();
  sensors.getAddress(thermometer, 0); 
  sensors.setResolution(thermometer, 12);

  // MQTT
  MQTTclient.setServer(mqtt_server, 1883);
  
  // Initialize DHT22.
  dht.begin();
  Serial.println("DHTxx Unified Sensor Example");
  // Print temperature sensor details.
  sensor_t sensor;
  dht.temperature().getSensor(&sensor);
  Serial.println("------------------------------------");
  Serial.println("Temperature");
  Serial.print  ("Sensor:       "); Serial.println(sensor.name);
  Serial.print  ("Driver Ver:   "); Serial.println(sensor.version);
  Serial.print  ("Unique ID:    "); Serial.println(sensor.sensor_id);
  Serial.print  ("Max Value:    "); Serial.print(sensor.max_value); Serial.println(" *C");
  Serial.print  ("Min Value:    "); Serial.print(sensor.min_value); Serial.println(" *C");
  Serial.print  ("Resolution:   "); Serial.print(sensor.resolution); Serial.println(" *C");  
  Serial.println("------------------------------------");
  // Print humidity sensor details.
  dht.humidity().getSensor(&sensor);
  Serial.println("------------------------------------");
  Serial.println("Humidity");
  Serial.print  ("Sensor:       "); Serial.println(sensor.name);
  Serial.print  ("Driver Ver:   "); Serial.println(sensor.version);
  Serial.print  ("Unique ID:    "); Serial.println(sensor.sensor_id);
  Serial.print  ("Max Value:    "); Serial.print(sensor.max_value); Serial.println("%");
  Serial.print  ("Min Value:    "); Serial.print(sensor.min_value); Serial.println("%");
  Serial.print  ("Resolution:   "); Serial.print(sensor.resolution); Serial.println("%");  
  Serial.println("------------------------------------");
  Serial.print  ("Min Delay:    "); Serial.print(sensor.min_delay / 1000); Serial.println("ms");
}

float sendTemperature(DeviceAddress deviceAddress)
{ 
  float tempC = sensors.getTempC(deviceAddress);

  minutes--;
  
  display.clear();
  display.setFont(ArialMT_Plain_16);

  Serial.print("temp: ");
  Serial.print(tempC);
  Serial.println("'C");

  if (minutes == 0)
  {
    minutes = PERIOD;
    // transmit data
    if (client.connect("www.golem.de", 80))
    {
      String cmd = "GET ";
      cmd += URL;
      cmd += tempC;
      cmd += " HTTP/1.0";
  
      client.println(cmd);
      client.println("Host: www.golem.de");
      client.println("Connection: close");
      client.println();

      display.drawString(0, 16, "sent!");
      display.display();
      delay(2000);

      Serial.println("sent");
      
      client.stop();    
    }
  }
  
  // countdown bar
  float progress = (float)minutes * (100.0f / 15.0f);
  display.drawProgressBar(0, 32, 120, 10, (int)progress);
  display.display();

  return tempC;
}

void loop()
{
  static unsigned long sensortime = 0;
  float tempC;
  char Buffer[8];
  int writtenBytes = 0;
  
  if (!MQTTclient.connected())
    reconnect();
  MQTTclient.loop();

  if(sensortime < millis())
  {
    sensortime = millis() + 60l * 1000l; // Minute
    
    sensors.requestTemperatures(); 
    tempC = sendTemperature(thermometer);
    
    dtostrf(tempC, 5, 2, Buffer);
    MQTTclient.publish("Temperature", Buffer);
    
    // display.drawString(0, 0, "Temp: " + String(tempC) + "°C" );

    // Get temperature event and print its value.
    sensors_event_t event;  
    dht.temperature().getEvent(&event);
    if (isnan(event.temperature))
    {
      Serial.println("Error reading temperature!");
    }
    else
    {
      Serial.print("Temperature: ");
      Serial.print(event.temperature);
      Serial.println(" *C");
        
      dtostrf(event.temperature, 5, 2, Buffer);
      MQTTclient.publish("TemperatureDHT22", Buffer);
      
      display.drawString(0, 0, "Temp: " + String(event.temperature) + "°C" );
    }
    // Get humidity event and print its value.
    dht.humidity().getEvent(&event);
    if (isnan(event.relative_humidity))
    {
      Serial.println("Error reading humidity!");
    }
    else
    {
      Serial.print("Humidity: ");      Serial.print(event.relative_humidity);
      Serial.println("%");
      
      display.drawString(0, 48, "Hum: " + String(event.relative_humidity) + "%");
      display.display();

      dtostrf(event.relative_humidity, 5, 2, Buffer);
      MQTTclient.setServer(mqtt_server, 1883);
      MQTTclient.publish("Humidity", Buffer);
    }
  }
  
  delay(1000); // slow down loop
}
