// WiFi Settings
const char* ssid = "ssid";
const char* password = "password";
char* espHostname = "BigRedButton01";

// MQTT Server
const char* mqtt_server = "mqtt broker hostname";

// MQTT Subscribes 
const char* mainTopic = "Flauschecke/#";
const char* SpeedTopic = "Flauschecke/Speed";

//Start Animation (1-3)
// 1=rainbowcolor, 2=rainbowcycle, 3=dreiercycle
const int Animation = 1;

// MQTT Last will and Testament
byte willQoS = 0;
const char* willTopic = "lwt/esp-bigredbutton01";
const char* willOnMessage = "online";
const char* willOffMessage = "offline";
const char* willClientID = "esp-bigredbutton01";
boolean willRetain = true;

// Neopixel Config
#define NeoPIN D5
#define NUM_LEDS 22

//Button
#define BUTTON D2
#define LED D3


