//mqtt 433mhz rc-switch gateway for NodeMCU V3
//currently 3 rc-switch-sockets working
//the 3 sockets are adressable with 'A','B','C'
//on is '1', off is '0'
//example: 'B0' switches the 2. socket off


#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <RCSwitch.h>

// Wlan-Daten: Name des Netzwerks (SSID) und Passwort
#define SSID "SSID"
#define PASSWORD "PSK"

WiFiClient client;

const char* mqtt_server = "ip address or FQDN";
PubSubClient MQTTclient(client);

long lastMsg = 0;
char msg[50];
int value = 0;

RCSwitch mySwitch = RCSwitch();
                        //maybe change the following codes for your needs
int switchRemote[2][7] = {{6149123, 6124547, 6118403, 4277588, 4280660, 4281428, 17748},{6149132, 6124556, 6118412, 4277585, 4280657, 4281425, 17745}}; //24bit codes 1. array off 2. on
int switchCurrentCode = 0;
int switchCurrentMode = 0; //off, on

void reconnect() {
  // Loop until we're reconnected
  while (!MQTTclient.connected())
  {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (MQTTclient.connect("rc-switchGateway"))
    {
      Serial.println("connected");
      MQTTclient.subscribe("rc-switch");
    }
    else
    {
      Serial.print("failed, rc=");
      Serial.print(MQTTclient.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

void connectWifi()
{
  int xpos = 0;
  
  WiFi.disconnect();
  WiFi.mode(WIFI_STA);
  WiFi.begin(SSID, PASSWORT);

  Serial.print("connect wifi");

  while (WiFi.status() != WL_CONNECTED)
  {
    Serial.print(".");
    delay(500);
  }

  Serial.println();
  Serial.print("wifi ip: ");
  Serial.println(WiFi.localIP());

  delay(3000);
}

void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println();

  if (payload[1] == '0') {
    switchCurrentMode = 0;
  } else if (payload[1] == '1') {
    switchCurrentMode = 1;
  }
  switch  (payload[0]) {
    case 'A':
      switchCurrentCode = switchRemote[switchCurrentMode][0];
      break;
    case 'B':
      switchCurrentCode = switchRemote[switchCurrentMode][1];
      break;
    case 'C':
      switchCurrentCode = switchRemote[switchCurrentMode][2];
      break;
    case 'D':
      switchCurrentCode = switchRemote[switchCurrentMode][3];
      break;
    case 'E':
      switchCurrentCode = switchRemote[switchCurrentMode][4];
      break;
    case 'F':
      switchCurrentCode = switchRemote[switchCurrentMode][5];
      break;
    case 'G':
      switchCurrentCode = switchRemote[switchCurrentMode][6];
      break;
  }
  mySwitch.send(switchCurrentCode, 24);
}

void setup() {
  Serial.begin(115200);
  Serial.println();

  connectWifi();

  // MQTT
  MQTTclient.setServer(mqtt_server, 1883);
  MQTTclient.setCallback(callback);

  // Transmitter is connected to Arduino Pin #D4
  mySwitch.enableTransmit(2);
  
  // put your setup code here, to run once:
  // initialize serial communication at 115200 bits per second:

}

void loop() {
  if (!MQTTclient.connected()) {
    reconnect();
  }
  MQTTclient.loop();

}

